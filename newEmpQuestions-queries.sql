-- 1) How many Devices do we currently have?

SELECT APPROX_DISTINCT(advertiser_id)
FROM device
WHERE created_on >= current_date - interval '90' day --change to device.created_on >= timestamp'2018-11-28' format
--8174440

--------------------------
-- 2) How many Devices added the last 30 days?  (Hint: check your answer against the Geckoboard)
SELECT COUNT(advertiser_id)
FROM Device
WHERE Device.created_on >= current_date - interval '30' day --change to device.created_on >= timestamp'2018-11-28' format
--2035127

-------------------------------------------
-- 3) How many of these Devices have at least one persona?
SELECT APPROX_DISTINCT(device_persona.advertiser_id) AS "device_count"
FROM device_persona JOIN device ON device_persona.advertiser_id = device.advertiser_id
WHERE device.created_on >= current_date - interval '30' day; --change to device.created_on >= timestamp'2018-11-28' format
-- returns: 2798354

-------------------------------------
-- 4) How many total Beacons have we detected?
SELECT approx_distinct(concat(cast(beacon_uuid as varchar), concat(cast(beacon_minor as varchar), cast(beacon_major as varchar))))
FROM event
WHERE year = 2018
        AND month = 12
        AND day IN (8, 9, 10, 11, 12)
        AND type_id = 2;
--RETURNS: 4070949

-------------------------------------- *error
-- 5) How many Beacons have we detected over the last 30 days?
-- ** how many of these do not have the UUID 6CA0C73C-F8EC-4687-9112-41DCB6F28879 or 66622E6D-652F-40CA-9E6F-6F7166616365 or 5993A94C-7D97-4DF7-9ABF-E493BFD5D000 (use a case insensitive search) ? 
SELECT approx_distinct(concat(cast(beacon_uuid AS varchar),
         concat(cast(beacon_minor AS varchar),
         cast(beacon_major AS varchar))))
FROM event
WHERE year = 2018
        AND month = 12
        AND day IN (12, 11, 10, 9, 8)
        AND type_id = 2
        AND lower(beacon_uuid) NOT IN (
        lower('CA0C73C-F8EC-4687-9112-41DCB6F28879'),
        lower('66622E6D-652F-40CA-9E6F-6F7166616365'),
        lower('5993A94C-7D97-4DF7-9ABF-E493BFD5D000'));
--1779531
-----------------------------------------
-- 6) How many Beacons do we have classified? (Hint: check your answer against the Geckoboard)
--assumes all beacons in BeaconClass table are classified
SELECT COUNT(DISTINCT(uuid))
FROM BeaconClass;
--returns: 5604

------------------------------------ ** change name to city
-- 7) What cities have the most Classified Beacons?
SELECT COUNT(concat(cast(beacon.uuid AS varchar),
         concat(cast(beacon.minor AS varchar),
         cast(beacon.major AS varchar)))) AS beacon_count, place.city
FROM public."BeaconClass" beacon, place_google place
WHERE beacon.place_id = place.id
GROUP BY place.city
ORDER BY beacon_count DESC
LIMIT 6;
--RETURNS:
-- 47743	null
-- 35299	Raleigh
-- 31979	Jacksonville
-- 12506	Holly Springs
-- 11524	Houston
--10481	San Antonio

-----------------------------------------
-- 8a) What Businesses/Places have the most beacon bumps over the last 15 days?
SELECT place_id,
         APPROX_DISTINCT(advertiser_id) AS device_count
FROM place_event
WHERE year = 2018
AND month = 12
AND day IN (9,10,11,12,13)
AND type_id = 2
GROUP BY  place_id 
ORDER BY device_count DESC
LIMIT 5;

--  	place_id	device_count
-- 1	1772521350435439930	1162
-- 2	1733418289461200342	1013
-- 3	1353144130972157261	995
-- 4	1790045224471364738	987
-- 5	1353599623662404874	844

-- 8b) What Businesses/Places have the most unique devices over the last 15 days? 
SELECT place_event.place_id, APPROX_DISTINCT(place_event.advertiser_id) AS device_count, place.name
FROM place_event JOIN place
ON place_event.place_id = place.id
WHERE place_event.year = 2018
AND place_event.month = 12 
AND place_event.day BETWEEN 10 AND 15
GROUP BY place.name, place_event.place_id
ORDER BY device_count DESC
LIMIT 5;

--  	place_id	device_count	name
-- 1	1877174035247269523	355652	Las Vegas Strip + Hotels
-- 2	1353144117869152150	150986	Mazel Stores 209
-- 3	1353144274056644197	150986	Lot Less Closeouts
-- 4	1353144378805191959	150378	One Sixteen Inc
-- 5	1353144092678162297	150378	Juniper Management Inc

---------------------------------------------
-- 9) What Business Chains have the most classified beacons? 
SELECT COUNT(t1.uuid), t2.chain_id, t3.name
FROM public."BeaconClass" t1, public."place_google" t2, public."chain" t3
where t1.place_id = t2.id
and t2.chain_id = t3.id
GROUP BY t2.chain_id, t3.name
ORDER BY COUNT(t1.uuid) DESC
LIMIT 5;

-- 27481	1353143788842779827	CVS Pharmacy
-- 20023	1353143788381406992	Target
-- 16168	1353143788624676349	Starbucks Coffee
-- 12111	1353143788557567278	Walmart
-- 8794	1353143788456904674	redbox

------------------------------------------------
-- 10) Over the last 15 days, what is our average number of daily active devices? (active = device has an event that day)
SELECT cast(avg(count_ids) AS integer)
FROM 
    (SELECT cast(APPROX_DISTINCT(distinct(advertiser_id))) AS count_ids
    FROM daily_device_stats
    WHERE year = 2018
            AND ( month = 12
            OR (month = 11
            AND day IN (29,30)) ) ) 
-- 41326724

-------------------------------------------------
-- 11) What are the top 5 apps over the last 30 days which contributed the most new devices with a home location, work location, other?
 -- work

SELECT o.name,
        approx_distinct(d.advertiser_id) AS device_count
FROM daily_device_app dda
JOIN device d
   ON dda.advertiser_id = d.advertiser_id
JOIN app a
   ON dda.app_id = a.id
JOIN org o
   ON a.org_id = o.id
WHERE dda.year = 2018
       AND ((dda.month = 11
       AND dda.day BETWEEN 28 AND 30)
       OR (dda.month = 12
       AND dda.day BETWEEN 1 AND 3))
AND d.created_on BETWEEN timestamp'2018-11-28' AND timestamp '2018-12-03'       
AND d.work_lat IS NOT NULL
GROUP BY  o.name
ORDER BY  device_count DESC LIMIT 5
-- 1	ModFx Labs	104145
-- 2	UberMedia	83048
-- 3	ScanBuy	19528
-- 4	StartApp	7897
-- 5	OneSignal	6895
--home

SELECT o.name,
        approx_distinct(d.advertiser_id) AS device_count
FROM daily_device_app dda
JOIN device d
   ON dda.advertiser_id = d.advertiser_id
JOIN app a
   ON dda.app_id = a.id
JOIN org o
   ON a.org_id = o.id
WHERE dda.year = 2018
       AND ((dda.month = 11
       AND dda.day
   BETWEEN 28
       AND 30)
       OR (dda.month = 12
       AND dda.day
   BETWEEN 1
       AND 3))
       AND d.created_on BETWEEN timestamp'2018-11-28' AND timestamp '2018-12-03' 
       AND d.pr_lat IS NOT NULL
GROUP BY  o.name
ORDER BY  device_count DESC LIMIT 5

-------------------------------------------------------------
-- 13) How many uniques devices did we share with X-mode(data-partner) yesterday?
SELECT org.name, org.id, shares.count, shares.day
        FROM auth."Organization" org INNER JOIN auth."RunningOrgDeviceCounts" shares
        ON org.id = shares.org_id
        WHERE shares.day = '2018-12-05'
        AND org.name LIKE 'x-mode'
--returns: 25231860








































































































































































































































































































































----------------------------------------------------------------
-- 14) What are the different beacon types according to Reveal ? (beacon_type field in "BeaconClass" table)
SELECT DISTINCT(beacon_type)
FROM "BeaconClass"
-- RETURNS:
-- Static Beacon
-- Multi Beacon
-- Unclassified
-- Custom Beacon
